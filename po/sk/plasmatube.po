# translation of plasmatube.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasmatube\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-07 00:58+0000\n"
"PO-Revision-Date: 2022-04-15 19:19+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: contents/ui/AccountPage.qml:14 contents/ui/AccountPage.qml:93
#: contents/ui/SettingsPage.qml:140
#, kde-format
msgid "Sign in"
msgstr ""

#: contents/ui/AccountPage.qml:21
#, kde-format
msgid "Successfully logged in as %1."
msgstr ""

#: contents/ui/AccountPage.qml:40
#, kde-format
msgid "Currently logged in as %1."
msgstr ""

#: contents/ui/AccountPage.qml:49 contents/ui/SettingsPage.qml:62
#, kde-format
msgid "Log out"
msgstr "Odhlásiť sa"

#: contents/ui/AccountPage.qml:59 contents/ui/SettingsPage.qml:68
#, kde-format
msgid ""
"Please visit the website to register with an invidious instance.\n"
"There is currently no API for registering."
msgstr ""

#: contents/ui/AccountPage.qml:76 contents/ui/SettingsPage.qml:100
#, kde-format
msgid "Username"
msgstr "Používateľské meno"

#: contents/ui/ChannelPage.qml:82
#, kde-format
msgid "Loading"
msgstr ""

#: contents/ui/components/BottomNavBar.qml:69
#: contents/ui/components/Sidebar.qml:60
#, kde-format
msgid "Videos"
msgstr "Videá"

#: contents/ui/components/BottomNavBar.qml:81
#: contents/ui/components/Sidebar.qml:73 contents/ui/SearchPage.qml:17
#: contents/ui/SearchPage.qml:174
#, kde-format
msgid "Search"
msgstr "Hľadať"

#: contents/ui/components/BottomNavBar.qml:92
#: contents/ui/components/Sidebar.qml:94 contents/ui/SettingsPage.qml:14
#: contents/ui/videoplayer/VideoControls.qml:176
#, kde-format
msgid "Settings"
msgstr "Nastavenia"

#: contents/ui/SearchPage.qml:70
#, kde-format
msgid "Sort By:"
msgstr ""

#: contents/ui/SearchPage.qml:74
#, kde-format
msgid "Rating"
msgstr ""

#: contents/ui/SearchPage.qml:85
#, kde-format
msgid "Relevance"
msgstr ""

#: contents/ui/SearchPage.qml:97
#, kde-format
msgid "Upload Date"
msgstr ""

#: contents/ui/SearchPage.qml:108
#, kde-format
msgid "View Count"
msgstr ""

#: contents/ui/SearchPage.qml:174
#, kde-format
msgid "No results"
msgstr "Žiadne výsledky"

#: contents/ui/SettingsPage.qml:25
#, kde-format
msgid "Invidious"
msgstr ""

#: contents/ui/SettingsPage.qml:29 contents/ui/SettingsPage.qml:79
#, kde-format
msgid "Instance"
msgstr "Inštancia"

#: contents/ui/SettingsPage.qml:44
#, kde-format
msgid "Account"
msgstr "Účet"

#: contents/ui/SettingsPage.qml:49
#, kde-format
msgid "Currently logged in as:"
msgstr ""

#: contents/ui/SettingsPage.qml:118
#, kde-format
msgid "Password"
msgstr ""

#: contents/ui/SettingsPage.qml:158
#, kde-format
msgid "About PlasmaTube"
msgstr ""

#: contents/ui/VideoGridItem.qml:149
#, kde-format
msgid "• %1 views • %2"
msgstr ""

#: contents/ui/VideoListItem.qml:146
#, kde-format
msgid " • %1 views"
msgstr ""

#: contents/ui/VideoListItem.qml:154
#, kde-format
msgid " • %1"
msgstr ""

#: contents/ui/videoplayer/VideoPlayer.qml:300
#, kde-format
msgid "Unsubscribe (%1)"
msgstr ""

#: contents/ui/videoplayer/VideoPlayer.qml:302
#, kde-format
msgid "Subscribe (%1)"
msgstr ""

#: contents/ui/videoplayer/VideoPlayer.qml:312
#, kde-format
msgid "Please log in to subscribe to channels."
msgstr ""

#: contents/ui/videoplayer/VideoPlayer.qml:332
#, kde-format
msgid "%1 views"
msgstr ""

#: contents/ui/videoplayer/VideoPlayer.qml:339
#, kde-format
msgid "%1 Likes"
msgstr ""

#: logincontroller.cpp:46 qinvidious/invidiousapi.cpp:97
#, kde-format
msgid "Username or password is wrong."
msgstr ""

#: logincontroller.cpp:49
#, kde-format
msgid "This instance has disabled the registration."
msgstr ""

#: main.cpp:47
#, kde-format
msgid "PlasmaTube"
msgstr ""

#: main.cpp:49 main.cpp:91
#, kde-format
msgid "YouTube client"
msgstr ""

#: main.cpp:51
#, kde-format
msgid "© Linus Jahn"
msgstr ""

#: main.cpp:52
#, kde-format
msgid "Linus Jahn"
msgstr ""

#: main.cpp:52
#, kde-format
msgid "Creator"
msgstr ""

#: main.cpp:53
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#: main.cpp:53
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: qinvidious/invidiousapi.cpp:187
#, kde-format
msgid "Server returned no valid JSON."
msgstr ""

#: qinvidious/invidiousapi.cpp:196
#, kde-format
msgid "Server returned the status code %1"
msgstr ""

#: videolistmodel.cpp:17
#, kde-format
msgid "Subscriptions"
msgstr ""

#: videolistmodel.cpp:19
#, kde-format
msgid "Invidious Top"
msgstr ""

#: videolistmodel.cpp:21
#, kde-format
msgid "Trending"
msgstr ""

#: videolistmodel.cpp:23
#, kde-format
msgid "Trending Gaming"
msgstr ""

#: videolistmodel.cpp:25
#, kde-format
msgid "Trending Movies"
msgstr ""

#: videolistmodel.cpp:27
#, kde-format
msgid "Trending Music"
msgstr ""

#: videolistmodel.cpp:29
#, kde-format
msgid "Trending News"
msgstr ""

#: videolistmodel.cpp:182
#, kde-format
msgid "Search results for \"%1\""
msgstr ""

#~ msgid "Fullscreen"
#~ msgstr "Celá obrazovka"

#~ msgid "Restore"
#~ msgstr "Obnoviť"

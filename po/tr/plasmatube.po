# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasmatube package.
#
# Emir SARI <emir_sari@icloud.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasmatube\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-07 00:58+0000\n"
"PO-Revision-Date: 2023-02-08 20:18+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.2\n"

#: contents/ui/AccountPage.qml:14 contents/ui/AccountPage.qml:93
#: contents/ui/SettingsPage.qml:140
#, kde-format
msgid "Sign in"
msgstr "Oturum Aç"

#: contents/ui/AccountPage.qml:21
#, kde-format
msgid "Successfully logged in as %1."
msgstr "%1 olarak başarıyla oturum açıldı."

#: contents/ui/AccountPage.qml:40
#, kde-format
msgid "Currently logged in as %1."
msgstr "Şu anda %1 olarak oturum açıldı."

#: contents/ui/AccountPage.qml:49 contents/ui/SettingsPage.qml:62
#, kde-format
msgid "Log out"
msgstr "Oturumu Kapat"

#: contents/ui/AccountPage.qml:59 contents/ui/SettingsPage.qml:68
#, kde-format
msgid ""
"Please visit the website to register with an invidious instance.\n"
"There is currently no API for registering."
msgstr ""
"Bireysel olarak kaydolmak için lütfen web sayfasını ziyaret edin.\n"
"Şu anda kayıt için bir uygulama arayüzü bulunmamaktadır."

#: contents/ui/AccountPage.qml:76 contents/ui/SettingsPage.qml:100
#, kde-format
msgid "Username"
msgstr "Kullanıcı Adı"

#: contents/ui/ChannelPage.qml:82
#, kde-format
msgid "Loading"
msgstr "Yükleniyor"

#: contents/ui/components/BottomNavBar.qml:69
#: contents/ui/components/Sidebar.qml:60
#, kde-format
msgid "Videos"
msgstr "Videolar"

#: contents/ui/components/BottomNavBar.qml:81
#: contents/ui/components/Sidebar.qml:73 contents/ui/SearchPage.qml:17
#: contents/ui/SearchPage.qml:174
#, kde-format
msgid "Search"
msgstr "Ara"

#: contents/ui/components/BottomNavBar.qml:92
#: contents/ui/components/Sidebar.qml:94 contents/ui/SettingsPage.qml:14
#: contents/ui/videoplayer/VideoControls.qml:176
#, kde-format
msgid "Settings"
msgstr "Ayarlar"

#: contents/ui/SearchPage.qml:70
#, kde-format
msgid "Sort By:"
msgstr "Şuna Göre Sırala:"

#: contents/ui/SearchPage.qml:74
#, kde-format
msgid "Rating"
msgstr "Derecelendirme"

#: contents/ui/SearchPage.qml:85
#, kde-format
msgid "Relevance"
msgstr "İlgi"

#: contents/ui/SearchPage.qml:97
#, kde-format
msgid "Upload Date"
msgstr "Yüklenme Tarihi"

#: contents/ui/SearchPage.qml:108
#, kde-format
msgid "View Count"
msgstr "Görüntülenme Sayısı"

#: contents/ui/SearchPage.qml:174
#, kde-format
msgid "No results"
msgstr "Sonuç yok"

#: contents/ui/SettingsPage.qml:25
#, kde-format
msgid "Invidious"
msgstr "Kırıcı"

#: contents/ui/SettingsPage.qml:29 contents/ui/SettingsPage.qml:79
#, kde-format
msgid "Instance"
msgstr "Örnek"

#: contents/ui/SettingsPage.qml:44
#, kde-format
msgid "Account"
msgstr "Hesap"

#: contents/ui/SettingsPage.qml:49
#, kde-format
msgid "Currently logged in as:"
msgstr "Oturum açılan hesap:"

#: contents/ui/SettingsPage.qml:118
#, kde-format
msgid "Password"
msgstr "Parola"

#: contents/ui/SettingsPage.qml:158
#, kde-format
msgid "About PlasmaTube"
msgstr "PlasmaTube Hakkında"

#: contents/ui/VideoGridItem.qml:149
#, kde-format
msgid "• %1 views • %2"
msgstr "• %1 görüntülenme • %2"

#: contents/ui/VideoListItem.qml:146
#, kde-format
msgid " • %1 views"
msgstr " • %1 görüntülenme"

#: contents/ui/VideoListItem.qml:154
#, kde-format
msgid " • %1"
msgstr " • %1"

#: contents/ui/videoplayer/VideoPlayer.qml:300
#, kde-format
msgid "Unsubscribe (%1)"
msgstr "Abonelikten Çık (%1)"

#: contents/ui/videoplayer/VideoPlayer.qml:302
#, kde-format
msgid "Subscribe (%1)"
msgstr "Abone Ol (%1)"

#: contents/ui/videoplayer/VideoPlayer.qml:312
#, kde-format
msgid "Please log in to subscribe to channels."
msgstr "Kanallara abone olmak için oturum açın."

#: contents/ui/videoplayer/VideoPlayer.qml:332
#, kde-format
msgid "%1 views"
msgstr "%1 görüntüleme"

#: contents/ui/videoplayer/VideoPlayer.qml:339
#, kde-format
msgid "%1 Likes"
msgstr "%1 beğeni"

#: logincontroller.cpp:46 qinvidious/invidiousapi.cpp:97
#, kde-format
msgid "Username or password is wrong."
msgstr "Kullanıcı adı veya parola yanlış."

#: logincontroller.cpp:49
#, kde-format
msgid "This instance has disabled the registration."
msgstr "Bu örnek, kaydı devre dışı bıraktı."

#: main.cpp:47
#, kde-format
msgid "PlasmaTube"
msgstr "PlasmaTube"

#: main.cpp:49 main.cpp:91
#, kde-format
msgid "YouTube client"
msgstr "YouTube istemcisi"

#: main.cpp:51
#, kde-format
msgid "© Linus Jahn"
msgstr "© Linus Jahn"

#: main.cpp:52
#, kde-format
msgid "Linus Jahn"
msgstr "Linus Jahn"

#: main.cpp:52
#, kde-format
msgid "Creator"
msgstr "Yaratıcı"

#: main.cpp:53
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Emir SARI"

#: main.cpp:53
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "emir_sari@icloud.com"

#: qinvidious/invidiousapi.cpp:187
#, kde-format
msgid "Server returned no valid JSON."
msgstr "Sunucu, geçerli bir JSON döndürmedi."

#: qinvidious/invidiousapi.cpp:196
#, kde-format
msgid "Server returned the status code %1"
msgstr "Sunucu, %1 durum kodunu döndürdü"

#: videolistmodel.cpp:17
#, kde-format
msgid "Subscriptions"
msgstr "Abonelikler"

#: videolistmodel.cpp:19
#, kde-format
msgid "Invidious Top"
msgstr "Kırıcı En Popüler"

#: videolistmodel.cpp:21
#, kde-format
msgid "Trending"
msgstr "Trendler"

#: videolistmodel.cpp:23
#, kde-format
msgid "Trending Gaming"
msgstr "Trend Oyunlar"

#: videolistmodel.cpp:25
#, kde-format
msgid "Trending Movies"
msgstr "Trend Filmler"

#: videolistmodel.cpp:27
#, kde-format
msgid "Trending Music"
msgstr "Trend Müzikler"

#: videolistmodel.cpp:29
#, kde-format
msgid "Trending News"
msgstr "Trend Haberler"

#: videolistmodel.cpp:182
#, kde-format
msgid "Search results for \"%1\""
msgstr "\"%1\" için arama sonuçları"

#~ msgid "Open Account Settings"
#~ msgstr "Hesap Ayarlarını Aç"

#~ msgid "Fullscreen"
#~ msgstr "Tam Ekran"

#~ msgid "Restore"
#~ msgstr "Geri Yükle"
